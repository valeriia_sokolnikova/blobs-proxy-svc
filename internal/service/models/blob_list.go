package models

import (
	"encoding/json"

	"gitlab.com/tokend/blobs-proxy-svc/internal/data"
	"gitlab.com/tokend/blobs-proxy-svc/resources"
)

func NewBlobResponseModel(blob data.Blob) resources.BlobResponse {
	result := resources.BlobResponse{
		Data: NewBlobModel(blob),
	}
	return result
}

func NewBlobListModel(blobs []data.Blob) []resources.Blob {
	result := make([]resources.Blob, len(blobs))
	for i, blob := range blobs {
		result[i] = NewBlobModel(blob)
	}
	return result
}

func NewBlobModel(blob data.Blob) resources.Blob {
	result := resources.Blob{
		Key: resources.Key{ID: blob.Id, Type: resources.BLOB},
		Attributes: resources.BlobAttributes{
			Blob: json.RawMessage(blob.Blob),
		},
		Relationships: resources.BlobRelationships{
			User: resources.Relation{
				Data: &resources.Key{
					ID:   string(blob.UserId),
					Type: resources.USERS}},
		},
	}
	return result
}
