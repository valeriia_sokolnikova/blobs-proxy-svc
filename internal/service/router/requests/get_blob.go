package requests

import (
	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"net/http"
)

type GetBlobRequest struct {
	Id string `url:"id"`
}

func NewGetBlobRequest(r *http.Request) (GetBlobRequest, error) {
	id := chi.URLParam(r, "id")
	result := GetBlobRequest{Id: id}
	return result, result.validate()
}

func (r *GetBlobRequest) validate() error {
	return validation.Errors {
		"id": validation.Validate(&r.Id, validation.Required),
	}.Filter()
}