package requests

import (
	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"net/http"
)

type DeleteBlobRequest struct {
	Id string `url:"id"`
}

func NewDeleteBlobRequest(r *http.Request) (DeleteBlobRequest, error) {
	id := chi.URLParam(r, "id")
	result := DeleteBlobRequest{Id: id}
	return result, result.validate()
}

func (r *DeleteBlobRequest) validate() error {
	return validation.Errors {
		"id": validation.Validate(&r.Id, validation.Required),
	}.Filter()
}