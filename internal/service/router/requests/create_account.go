package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/blobs-proxy-svc/resources"
)

func NewCreateAccountRequest(r *http.Request) (resources.CreateAccountResponse, error) {
	var request resources.CreateAccountResponse
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}
	return request, nil
}
