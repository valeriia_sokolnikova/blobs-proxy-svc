package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/blobs-proxy-svc/resources"
)

type CreateBlobRequest struct {
	Data resources.Blob
}

func NewAddBlobRequest(r *http.Request) (CreateBlobRequest, error) {
	request := CreateBlobRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}
	return request, request.validate()
}

func (r *CreateBlobRequest) validate() error {
	return validation.Errors{
		"/data/type":                  validation.Validate(&r.Data.Type, validation.Required, validation.In(resources.BLOB)),
		"/data/relationships/user_id": validation.Validate(&r.Data.Relationships.User.Data.ID, validation.Required),
	}.Filter()
}
