package router

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/blobs-proxy-svc/internal/config"
	"gitlab.com/tokend/blobs-proxy-svc/internal/data/pg"
	"gitlab.com/tokend/blobs-proxy-svc/internal/service/router/handlers"
	"gitlab.com/tokend/blobs-proxy-svc/internal/service/types"
)

type routerAPI struct {
	cfg config.Config
}

func NewRouterAPI(cfg config.Config) types.Service {
	return &routerAPI{
		cfg: cfg,
	}
}

func (s *routerAPI) Run(ctx context.Context) error {
	router := router(s.cfg)

	if err := s.cfg.Copus().RegisterChi(router); err != nil {
		return errors.Wrap(err, "cop failed")
	}

	return http.Serve(s.cfg.Listener(), router)
}

func router(cfg config.Config) chi.Router {
	r := chi.NewRouter()
	log := cfg.Log().WithFields(map[string]interface{}{
		"service": "users-api",
	})

	r.Use(
		ape.RecoverMiddleware(log),
		ape.LoganMiddleware(log), // this line may cause compilation error but in general case `dep ensure -v` will fix it
		ape.CtxMiddleware(
			handlers.CtxLog(log),
			handlers.CtxDB(pg.NewBlobQ(cfg.DB())),
			handlers.CtxHorizon(cfg.Horizon()),
		),
	)

	r.Route("/blobs", func(r chi.Router) {
		r.Post("/", handlers.CreateBlob)
		r.Route("/{id}", func(r chi.Router) {
			r.Delete("/", handlers.DeleteBlob)
			r.Get("/", handlers.GetBlob)
		})
		r.Get("/", handlers.GetBlobList)
	})
	r.Route("/accounts", func(r chi.Router) {
		r.Post("/", handlers.CreateAccount)
	})
	return r
}
