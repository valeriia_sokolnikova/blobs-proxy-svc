package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	//logger := Log(r)
	account, _ := keypair.Random()
	signers := []xdrbuild.SignerData{
		{
			PublicKey: account.Address(),
			RoleID:    1,
			Weight:    1000,
			Identity:  0,
			Details:   Details{},
		},
	}
	signer := keypair.MustParseSeed("SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4")
	tx := xdrbuild.
		NewBuilder("TokenD Developer Network", int64(24*time.Hour)).
		Transaction(signer).
		Sign(signer)
	tx.Op(&xdrbuild.CreateAccount{
		Destination: account.Address(),
		RoleID:      1,
		Signers:     signers,
	})

	str, _ := tx.Marshal()

	// Send to horizon
	// 1. build horizon Connector
	// 2. Submit

	result := Horizon(r).Submitter().Submit(r.Context(), str)

	err := convertTXSubmitError(result)
	if err != nil {
		//logger.WithError(err).Info("error")
		return
	}
}

type Details map[string]interface{}

func (d Details) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}(d))
}

func convertTXSubmitError(s horizon.SubmitResult) (err error) {
	badRequests := map[string]struct{}{
		"op_invalid_destination": {},
		"op_already_exists":      {},
	}

	if len(s.OpCodes) > 0 {
		// only create account op codes are handled
		opCode := s.OpCodes[0]
		if _, ok := badRequests[opCode]; ok {
			return validation.Errors{
				"/data/attributes/account_id": errors.New(
					fmt.Sprintf("'%s' op code received on create account op", opCode),
				),
			}
		}
	}

	return checkTxResult(s)
}

func checkTxResult(result horizon.SubmitResult) error {
	switch result.TXCode {
	case "tx_success":
		return nil
	case "tx_failed":
		return validation.Errors{"create account tx": errors.New(fmt.Sprint("transaction failed with op codes: ", result.OpCodes))}
	default:
		return validation.Errors{"create account tx": errors.New(fmt.Sprint("transaction failed with tx code: ", result.TXCode))}
	}
}
