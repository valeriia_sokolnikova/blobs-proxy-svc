package handlers

import (
	"crypto/sha1"
	"encoding/hex"
	"net/http"

	"gitlab.com/tokend/blobs-proxy-svc/internal/service/models"

	requests2 "gitlab.com/tokend/blobs-proxy-svc/internal/service/router/requests"

	"github.com/jmoiron/sqlx/types"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/tokend/blobs-proxy-svc/internal/data"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	logger := Log(r)
	request, err := requests2.NewAddBlobRequest(r)
	if err != nil {
		logger.WithError(err).Info("bad request")
		return
	}
	h := sha1.New()
	h.Write(request.Data.Attributes.Blob)
	db := DB(r)
	existingBlob, err := db.FilterByID(hex.EncodeToString(h.Sum(nil))).Get()
	if existingBlob != nil {
		w.WriteHeader(http.StatusConflict)
		return
	}
	blob := data.Blob{
		Id:     hex.EncodeToString(h.Sum(nil)),
		UserId: request.Data.Relationships.User.Data.ID,
		Blob:   types.JSONText(request.Data.Attributes.Blob)}
	blob, err = db.Insert(blob)
	if err != nil {
		logger.WithError(err).Info("failed to create blob")
		ape.Render(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	ape.Render(w, models.NewBlobModel(blob))
	return
}
