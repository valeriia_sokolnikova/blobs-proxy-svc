package handlers

import (
	"fmt"

	"gitlab.com/distributed_lab/ape"

	requests2 "gitlab.com/tokend/blobs-proxy-svc/internal/service/router/requests"

	//"github.com/go-chi/chi"
	"net/http"
	//"strconv"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	logger := Log(r)
	request, err := requests2.NewDeleteBlobRequest(r)
	if err != nil {
		logger.WithError(err).Info("bad request")
		return
	}
	fmt.Println(request.Id)
	db := DB(r)
	err = db.FilterByID(request.Id).Delete()
	if err != nil {
		logger.WithError(err).Info("failed to delete blob")
		ape.Render(w, err)
		return
	}
	w.WriteHeader(http.StatusNoContent)
	return
}
