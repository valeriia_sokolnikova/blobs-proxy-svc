package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/tokend/blobs-proxy-svc/internal/data"
	"gitlab.com/tokend/blobs-proxy-svc/internal/service/models"
	requests2 "gitlab.com/tokend/blobs-proxy-svc/internal/service/router/requests"
	"gitlab.com/tokend/blobs-proxy-svc/resources"
)

func GetBlobList(w http.ResponseWriter, r *http.Request) {
	logger := Log(r)
	request, err := requests2.NewGetBlobListRequest(r)
	if err != nil {
		logger.WithError(err).Info("bad request")
		return
	}
	db := DB(r)
	if len(request.FilterUserId) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	blobQ := db.New()
	applyBlobsFilters(blobQ, request)

	blobs, err := blobQ.Select()
	if err != nil {
		logger.WithError(err).Info("failed to get blob list")
		ape.Render(w, err)
		return
	}
	response := resources.BlobListResponse{
		Data: models.NewBlobListModel(blobs),
	}
	ape.Render(w, response.Data)
	return
}

func applyBlobsFilters(q data.BlobQ, r requests2.GetBlobListRequest) {

	if len(r.FilterUserId) > 0 {
		q.FilterByUserID(r.FilterUserId...)
	}
}
