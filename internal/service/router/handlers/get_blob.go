package handlers

import (
	"net/http"

	"gitlab.com/tokend/blobs-proxy-svc/internal/service/models"

	"gitlab.com/distributed_lab/ape"
	requests2 "gitlab.com/tokend/blobs-proxy-svc/internal/service/router/requests"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	logger := Log(r)
	request, err := requests2.NewGetBlobRequest(r)
	if err != nil {
		logger.WithError(err).Info("bad request")
		return
	}
	db := DB(r)
	blob, err := db.FilterByID(request.Id).Get()
	if blob == nil {
		logger.WithError(err).Info("Blob with such id doesn't exist")
		return
	}
	if err != nil {
		logger.WithError(err).Info("failed to get blob from db")
		ape.Render(w, err)
		return
	}
	ape.Render(w, models.NewBlobModel(*blob))
	w.WriteHeader(http.StatusOK)
	return
}
