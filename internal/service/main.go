package service

import (
    "context"
    "gitlab.com/tokend/blobs-proxy-svc/internal/config"
    "gitlab.com/tokend/blobs-proxy-svc/internal/service/router"
    "gitlab.com/tokend/blobs-proxy-svc/internal/service/types"
    "sync"
)

func runService(service types.Service, wg *sync.WaitGroup) {
    wg.Add(1)
    go func() {
        defer func() {
            wg.Done()
        }()

        _ = service.Run(context.Background())
    }()
}

func Run(cfg config.Config) {
    wg := &sync.WaitGroup{}

    routerApi := router.NewRouterAPI(cfg)
    runService(routerApi, wg)

    wg.Wait()
}
