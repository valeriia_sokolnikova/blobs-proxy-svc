-- +migrate Up

create table blobs (
    id character varying primary key,
    user_id bigint not null,
    blob jsonb default '{}'::jsonb not null
);

-- +migrate Down

drop table blobs;