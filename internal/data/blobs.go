package data

import (
	"github.com/jmoiron/sqlx/types"
)

type BlobQ interface {
	Get() (*Blob, error)
	Select() ([]Blob, error)
	Insert(value Blob) (Blob, error)
	Delete() error

	FilterByID(ids ...string) BlobQ
	FilterByUserID(ids ...string) BlobQ
	New() BlobQ
}

type Blob struct {
	Id     string         `db:"id" structs:"id"`
	UserId string         `db:"user_id" structs:"user_id"`
	Blob   types.JSONText `db:"blob" structs:"blob"`
}
