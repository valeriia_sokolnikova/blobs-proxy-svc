package pg

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/blobs-proxy-svc/internal/data"
)

const userTableName = "blobs"

func NewBlobQ(db *pgdb.DB) data.BlobQ {
	return &blobQ{
		db:  db,
		sql: sq.StatementBuilder,
	}
}

type blobQ struct {
	db  *pgdb.DB
	sql sq.StatementBuilderType
}

func (m *blobQ) New() data.BlobQ {
	return NewBlobQ(m.db)
}

func (q *blobQ) Get() (*data.Blob, error) {
	var result data.Blob
	stmt := q.sql.Select("*").From(userTableName)
	err := q.db.Get(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (q *blobQ) Select() ([]data.Blob, error) {
	var result []data.Blob
	stmt := q.sql.Select("*").From(userTableName)
	err := q.db.Select(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (q *blobQ) Insert(value data.Blob) (data.Blob, error) {
	clauses := structs.Map(value)
	var result data.Blob
	stmt := sq.Insert(userTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return data.Blob{}, err
	}
	return result, nil
}

func (q *blobQ) Delete() error {
	err := q.db.Exec(q.sql.Delete(userTableName))
	if err != nil {
		return err
	}
	return nil
}
func (q *blobQ) FilterByID(ids ...string) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"id": ids})
	return q
}

func (q *blobQ) FilterByUserID(ids ...string) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"user_id": ids})
	return q
}
