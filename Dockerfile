FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/blobs-proxy-svc

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/blobs-proxy-svc gitlab.com/tokend/blobs-proxy-svc


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/blobs-proxy-svc /usr/local/bin/blobs-proxy-svc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["blobs-proxy-svc"]
